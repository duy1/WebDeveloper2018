## Features

### Chapter 5 - Advanced HTML5
 - Introduce HTML5  `form`, different types of `input`s and form validation (check out the  input-and-form-html5.png for more details).
 - [Differences](https://www.diffen.com/difference/GET-vs-POST-HTTP-Requests) between POST and GET method, and [the standard uses](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods) of http method ( related, optional )
- HTML5 semantics elements and [how it improves SEO](https://www.inboundnow.com/html5-semantic-elements-mean-seo/)( You may need to [read](https://medium.com/@cabot_solutions/web-crawlers-everything-you-need-to-know-6dce26ee8ad8) or [watch](https://www.youtube.com/watch?v=CDXOcvUNBaA) about Web crawlers first to get a better understanding.
- Recheck your html5  understandings by [a short quiz](https://www.w3schools.com/html/html_quiz.asp)
