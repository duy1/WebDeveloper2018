### Chapter 11 - DOM Manipulation
-  What is [DOM and DOM manipulation](https://www.w3schools.com/Js/js_htmldom.asp)  ?
- The difference difference between `querySelector` and `document.getElementBy` and their [uses](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelector).
- Improve performance by caching selectors in variables
- Why using innerHTNL is [dangerous](https://stackoverflow.com/questions/16860287/security-comparison-of-eval-and-innerhtml-for-clientside-javascript).
- A cool project about DOM manipulation, checkout the child folder.
