## Features

### Chapter 1 - Introduction:
- The [4-step guide to start](https://github.com/zero-to-mastery/start-here-guidelines "4-step guide to start")
- Download Discord ([link here](https://discordapp.com/ "link here")) to access the community of self-taught web developers and beginner just like you.
- Join a Open Source project of community named [Zero to Mastery](https://github.com/zero-to-mastery "Zero to Mastery")
- Articles that might inspire the new learners [Learn to code 2018 - Get hired and have fun](https://hackernoon.com/learn-to-code-in-2018-get-hired-and-have-fun-along-the-way-b338247eed6a "Learn to code 2018 - Get hired and have fun")
