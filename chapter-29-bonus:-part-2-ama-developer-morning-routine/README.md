
### Chapter 29: Bonus Part 2 + Extras
- Questions asked:
  1. Questions interview for a React developer:
     - The benefits of React ?  What does it solve
     - The short coming of React ?
  2. Blogs to read:
     - [Medium](https://medium.com/)
     - [Software Engineering Daily](https://softwareengineeringdaily.com/)
  3. To pass an inteview.
	 - Convince the employer that you're gonna help them out, you will work very very hard than anyone else.
  4. Books recomendation:
	 - 4-hour workweek.
	 - [So Good They Can't Ignore You](https://www.amazon.com/Good-They-Cant-Ignore-You/dp/1455509124).
  5. What is the meaning of life?
	 - Provide more values than you capture
- A Developer's Morning Routine:
	1. Check [Medium top 10 articles of the day](https://medium.com/browse/top) and read any ones related to tech  

	2. Read the top articles (over 100 upvotes) that interest you on [Haker News](https://news.ycombinator.com/)  

	3. Listen to a daily podcast on [Software Engineering Daily](https://softwareengineeringdaily.com/)  

	4. Check out cool products for inspiration and developer tools with over 200 upvotes on [Product Hunt](https://www.producthunt.com/) from the previous day.  

	5. Sign up for the [Javascript weekly newsletter](http://javascriptweekly.com/) and read through that every Friday.
