## Features

### Chapter 3 - How the internet works:

- The [history](https://www.vox.com/a/internet-maps "history") of the internet and the World Wide Web (www)
- Appoach the insight of [Computers Networks](https://youtu.be/3QhU9jd03a0?list=PL8dPuuaLjXtNlUrzyH5r6jN9ulIgZBpdo "Computers Networks,") the [Internet](https://youtu.be/AEaKrq3SpW8?list=PL8dPuuaLjXtNlUrzyH5r6jN9ulIgZBpdo "Internet") and the [WWW](https://youtu.be/guvsH5OFizE?list=PL8dPuuaLjXtNlUrzyH5r6jN9ulIgZBpdo "WWW")
- HTML, CSS, JavaScript and their roles in presenting the web.
- Try adding HTML, CSS, and run JavaScript on Tim Berner Lee - the father of WWW  - 's [website ](http://info.cern.ch/hypertext/WWW/TheProject.html "website ").
