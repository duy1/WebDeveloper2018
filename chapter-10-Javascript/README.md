### Chapter 10 - Introduction to Javascript.
-  What is Javascript and ECMAScript ?  Are they different ? Why  can they be used interchangeably ? Here is the [answer](https://medium.freecodecamp.org/whats-the-difference-between-javascript-and-ecmascript-cba48c73a2b5).
- Playaround the browser console with maths and comparisions and write some simple commands.
- Basics Javascript things like type, variables, functions, ...
- Javascript terminologies.
- Javascript loop and iteration. [More details](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration)
- Javascript [Keywords](https://www.w3schools.com/Js/js_reserved.asp) ( or Reserved words )
- Check out the SectionOutline included in the project to review what you have learnt through out this chapter.
- And don't forget about exercises.
