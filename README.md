
# Web Developement 2018 - Path to Junior
### Chapter 1 - Introduction
#### - Course Overview
#### - Meet the community
#### - Join Our Open Source Projects
#### - Optional the Articles That Inspired this course.
### Chapter 2: How the internet works
#### - Browsing the web
#### - Breaking google
#### - The internet backbones
#### - Traceroute
#### - DEVELOPER FUNDAMENTALS I
#### - What does a developer do ?
### Chapter 3: History Of The Web
#### - WWW vs Internet ?
#### - HTML, JavaScript and CSS?
#### - Developer History
### Chapter 4: HTML 5
#### - Build your first website
#### - IDEs used
#### - How to ask question
#### - HTML tags
### Chapter 5: Advanced HTML 5
#### - HTML forms
#### - HTML vs HTML5
#### - Copy a website.
#### - HTML Challenge
### Chapter 6: CSS
#### - CSS properties
#### - CSS Selectors
#### - Text and fonts
#### - Images in CSS
#### - Box model
#### - CSS quiz.
### Chapter 7: Advanced CSS
#### - Minify CSS
#### - Flexbox
#### - CSS 3
#### - Responsive UI
### Chapter 8: Bootstrap 4, Templates, And Building Your Startup Landing Page
#### - Bootstrap Introduction
#### - Bootstrap 4
#### - Bootstrap grid
#### - Create landing page app and put it online
#### - Developement Fundamental
#### - Resources for free template
### Chapter 9: Career Of A Web Developer - Build your first website
#### - Career Of A Web Developer
#### - Developement Fundamental
### Chapter 10: Javascript
#### -  Introduction to Javascript
#### - What is Javascript
#### - Variables
#### - Control flow
#### - Functions
#### - Data Structures: Arrays
#### - Data Structures: Objects
#### - Simple Facebook App
#### - Loops
#### - Keywords
#### - Outline Documents
### Chapter 11: DOM Manipulation
#### - Document Object Model
#### - DOM Selectors
#### - DOM Events
#### - Callback Functions
#### - Background Generator porject.
#### - jQuerry
#### - Developement Fundamental
### Chapter 12: Advanced Javascript
#### - Scope
#### - Advanced Flow Control
#### - ES5 and ES6
#### - Advanced Functions
#### - Advanced Arrays
#### - Advanced Objects
#### - ES7
#### - ES8
#### - How Javascript works
#### - Modules
### Chapter 13: Command line
#### - Using the terminal
### Chapter 14: Developer Environment
#### - Sublime text 3
#### - Customize Sublime text 3
#### - Terminal
#### - Customize Terminal
### Chapter  15: Git + Github + Open Source Projects
#### - Installing Git
#### - Exercise: Helping A Developer
#### - Terminal Setup
#### - Contributing to Open Source
#### - Portfolio Website for Recruiters

### Chapter  16: A Day In The Life Of A Developer - Installing Git
#### - A Typical Day
#### - Exercise: Helping A Developer

### Chapter  17: NPM + NPM Scripts
#### - Introduction To NPM - Github
#### - Setting Up NPM and package.json
#### - Troubleshoot: Installing NPM and Node.js
#### - Installing And Using Packages
### Chapter  18: React.js + Redux
#### - Introduction To React.js
#### - create-react-app
#### - Your First React Component
#### - Building A React App 1
#### - Building A React App 2
#### - Building A React App 3
#### - Building A React App 4
#### - Building A React App 5
#### - Building A React App 6
#### - React Review
#### - Quick Note About Redux
#### - Error Boundary In React
#### - State management
#### - Why Redux?
#### - Installing Redux
#### - Redux Actions and Reducers
#### - Redux Store and Provider
#### - Redux connect()
#### - Redux Middleware
#### - Redux Async Actions
#### - Redux Project Structures
#### - Popular Tool For React + Redux
#### - Project Files - Redux
### Chapter  19:
### HTTP/JSON/AJAX + Asynchronous Javascript - Installing Git
#### - HTTP/HTTPS
#### - JSON
#### - JSON vs Form Data
#### - AJAX
#### - Promises
#### - ES8 - Async Await
#### - Exercise: ES8 - Async Await
### Chapter  20: Backend Basics
#### - Backend Basics
### Chapter  21: APIs
#### - Introduction to APIs
#### - Exercise: Web App Using Star Wars API
#### - Optional Exercise: Speech Recognition
### Chapter  22: FINAL PROJECT: SmartBrain Front-End
#### - What We Are Building
#### - Building Our Components
#### - Note: Updated Clarifai API: FACE_DETECT_MODEL
#### - Image Recognition API
#### - Face Detection Box
#### - Sign In Form And Routing
#### - Optional: Advanced setState()

### Chapter  23: Node.js + Express.js
#### - Introduction To Node.js
#### - Running script.js In Node
#### - Modules In Node
#### - Types of Modules
#### - Building a Server
#### - Introduction to Express.js
#### - Express Middleware
#### - Postman
#### - RESTful APIs
#### - Exercise: Santa's Node Helper4:02
### Chapter  24: FINAL PROJECT: SmartBrain Back-End -- Server
#### - Setting Up Our Server
#### - /signin and /register
#### - /profile/:id and /image
#### - Storing User Passwords
#### - Resource: Storing User Passwords Securely
#### - Connecting To Our Front-End
#### - Resource: CORS
#### - Registering Users
#### - User Profile Update
### Chapter  25: Databases
#### - Introduction To Databases
#### - Installing PostgreSQL
#### - Resources: Installing PostgreSQL
#### - SQL: Create Table
#### - SQL: Insert Into + Select
#### - SQL: Alter Table + Update
#### - SQL: Conditional Selections
#### - SQL: Functions
#### - Joining Tables Part 1
#### - Joining Tables Part 2
#### - SQL: Delete From + Drop Table
### Chapter  26: FINAL PROJECT: SmartBrain Back-End -- Database
#### - Setting Up Your Database
#### - Connecting To The Database
#### - Registering A User Part 1
#### - Registering A User Part 2
#### - Getting User Profiles
#### - Updating Entries
#### - Sign In
#### - Putting It All Together
#### - What's Next?
### Chapter  27: Production + Deployment
#### - Code Review
#### - Security Review
#### - Environmental Variables
#### - Deploying On Heroku 1
#### - Deploying On Heroku 2
#### - Deploying On Heroku 3
#### - Deploying On Heroku 4
### Chapter  28: Where To Go From Here?
#### - Resource: Interviewing
#### - My Advice On Interviewing
#### - My Advice On Getting Hired
#### - My Advice On Resume/Portfolio/LinkedIn
#### - My Advice On Being a Junior Developer
#### - My Advice On How To Become A Senior Developer
### Chapter  29: Bonus: Part 2 + Extras
#### - Ask Me Anything
#### - A Developer's Morning Routine
