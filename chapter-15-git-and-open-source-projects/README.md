### Chapter 15: Git + Github + Open Source Projects
- What is [Git](https://git-scm.com/).
- Some basic git [commands](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html).
- Git [pro book](https://git-scm.com/book/en/v2).
