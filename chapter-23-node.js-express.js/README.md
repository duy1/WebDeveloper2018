
### Chapter 23: Node.js + Express.js.
-  What is [Node.js](https://www.w3schools.com/nodejs/nodejs_intro.asp).
- [Run](https://youtu.be/HEW7Ykw7UgQ) Javascript code without browsers.
- Keeps track of [trends](https://stateofjs.com/).
- [Config](https://docs.npmjs.com/files/package.json) `package.json` file nicely so everything turn up right.
- [Express guide](https://expressjs.com/en/guide/routing.html).
- Using [Postman](https://www.getpostman.com/) for testing requests and response.
- Santa node helper problem([Github link](https://github.com/duyphaphach/santa-node-helper)).
