### Chapter 27: Production + Deployment.
- [Steps](https://devcenter.heroku.com/articles/getting-started-with-nodejs#deploy-the-app) to deploy backend to heroku.
- Troubleshooting:
  - [Conflict lock file](https://help.heroku.com/0KU2EM53/why-is-my-node-js-build-failing-because-of-conflicting-lock-files).
  - Can't not detect [build pack](https://stackoverflow.com/questions/9794413/failed-to-push-some-refs-to-githeroku-com).
- Steps to setup [database](https://dashboard.heroku.com/provision-addon?addonServiceId=6c67493d-8fc2-4cd4-9161-4f1ec11cbe69&planId=062a1cc7-f79f-404c-9f91-135f70175577) for heroku deployed app.
- [Steps](https://dashboard.heroku.com/apps/smartbrain-frontend111/deploy/heroku-git) to deploy frontend
