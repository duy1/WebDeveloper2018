### Chapter 19: HTTP/JSON/AJAX + Asynchronous Javascript.
- What is [HTTP](https://developer.mozilla.org/en-US/docs/Web/HTTP) ?
- Understand the [URL encoding](https://www.w3schools.com/tags/ref_urlencode.asp).
- [JSON](https://www.w3schools.com/js/js_json_intro.asp), [AJAX](https://www.w3schools.com/xml/ajax_intro.asp), and [Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise).
- Communicate with server using [browser API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch) `fetch()`
