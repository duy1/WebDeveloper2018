### Chapter 28: Where to go from here.
- Some resources for interview:
  1. [https://haseebq.com/how-to-break-into-tech-job-hunting-and-interviews/](https://haseebq.com/how-to-break-into-tech-job-hunting-and-interviews/)  
  2. [http://seldo.com/weblog/2014/08/26/you_suck_at_technical_interviews](http://seldo.com/weblog/2014/08/26/you_suck_at_technical_interviews)  
  3. [Offer Negotiation](https://medium.freecodecamp.org/how-not-to-bomb-your-offer-negotiation-c46bb9bc7dea)  
  4. [https://medium.freecodecamp.org/5-key-learnings-from-the-post-bootcamp-job-search-9a07468d2331](https://medium.freecodecamp.org/5-key-learnings-from-the-post-bootcamp-job-search-9a07468d2331)  
  5. [https://blog.devmastery.com/how-to-win-the-coding-interview-71ae7102d685](https://blog.devmastery.com/how-to-win-the-coding-interview-71ae7102d685)
 - Never call yourself a [*Junior Developer*](https://medium.com/@andreineagoie/dont-be-a-junior-developer-608c255b3056)
 - [How](https://hackernoon.com/developers-edge-how-to-become-a-senior-developer-f1ec1738cf45) to become a senior developer.
