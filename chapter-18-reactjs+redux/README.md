### Chapter 18: React.js + Redux
- How to [create](https://reactjs.org/docs/create-a-new-react-app.html) a Reactjs App easily as chicken cake.
- Some good packages that supports development besides which go with creat-react-app:
  - For Atom: language-babel(syntax highlighting).
  - For React: react-hot-loader(hot-reloading), tachyons or react-bootstrap ( styling and layout )
 - How to [include](https://css-tricks.com/snippets/css/using-font-face/) fonts in our website.
 - [Building](https://medium.com/@sgroff04/2-minutes-to-learn-react-16s-componentdidcatch-lifecycle-method-d1a69a1f753) a Error Boundry in React.
 - [Interchangeably use](https://stackoverflow.com/questions/30668326/what-is-the-difference-between-using-constructor-vs-getinitialstate-in-react-r) of constructor and initialiseState.
 - [Digest](https://redux.js.org/basics) the Redux.
 - Some tools working well with reacts: React router, Ramda, Lodash, Glamorous, Styled-components, Css modules, Gatsby.js, Next.js 5, Semantics UI, Material UI, many many google please.

 - Link to [project](https://github.com/aneagoie/robofriends-redux.git).
