
### Chapter 24:  FINAL PROJECT: SmartBrain Back-End -- Server
Libraries need:
- Express: `yarn add express`.
  - Simplify http requests and APIs. [Learn more](https://expressjs.com/en/4x/api.html#router)
- Body Parser: `yarn add body-parser`.
  - Provide middlewares used for reformating data from requests. [Details](https://github.com/expressjs/body-parser).
- Cors: `yarn add cors`.
  - Middleware user for making [Cross-origin requests](https://youtu.be/rlnhiwN8AnU).
  - Anatomy of an [HTTP Transaction](https://nodejs.org/en/docs/guides/anatomy-of-an-http-transaction/).
Link to [project](https://github.com/duyphaphach/smart-brain-api)
