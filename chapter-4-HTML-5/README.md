## Features

### Chapter 4 - HTML 5:
- Choose for yourself online a Text Editor that support Web Developing
	- [Atom](https://atom.io/ "Atom").
	- [Sublime Text](https://www.sublimetext.com/3)
	- [Visual Studio Code](https://code.visualstudio.com/ "Visual Studio Code")
- Two very useful from basics to advanced knowledge resources for every Web Developer
	- [Mozilla Developer Network](https://developer.mozilla.org/en-US/ "Mozilla Developer Network")
	- [W3Schools Online Web Tutorials](https://www.w3schools.com "W3Schools Online Web Tutorials")
- Learn how to ask question
	- [Rubber Duck Debugging](https://rubberduckdebugging.com/ "Rubber Duck Debugging") Sound weird but an effective way to debug
	- Google and [StackOverFlow](https://stackoverflow.com/ "StackOverFlow")
- Semantics HTML meaning which is part of HTML 5
- Some important HTML 5 tags.
- Understand relative path and absolute path.
