### Chapter 17: NPM and NPM Scripts
- What is [npm](https://docs.npmjs.com/getting-started/what-is-npm) ?
- [Start](https://docs.npmjs.com/cli/init) a project with npm and package.json.
- [NVM](https://www.sitepoint.com/quick-tip-multiple-versions-node-nvm/) for switching between version of nodejs for each individual project.
- Try install a random package on the project.
