
### Chapter 26: Setting up database for smart brain app.
Steps to reproduce:
- `sudo -u postgres psql` to login DBMS as postgres user.
- `GRANT ALL GRANT ALL PRIVILEGES ON ALL TABLES TO duypeesea(ubuntu username);` to make sure we can access database without postgres acc.
- `CREATE DATABASE smartbrain` create new database named smartbrain
- Create new table `users` in smart brain:
`CREATE TABLE users (
		 id serial PRIMARY KEY,
		 name varchar(100),
		 email text UNIQUE NOT NULL,
		 entries BIGINT DEFAULT 0,
		 joined TIMESTAMP NOT NULL
	);`
	- Create new table `login`:
`CREATE TABLE login (
		 id serial PRIMARY KEY,
		 hash varchar(100) NOT NULL,
	);`
- Now we are having a database, continue to connect it with backend.
  - `yarn add knex` A SQL query builder to query to database
  - `yarn add pg` A [collection](https://node-postgres.com/) of nodejs modules for interact with PostgreSQL databases.
  - Learn how to [build querry](https://knexjs.org/#Builder) with `knex` to manipulate database. 
