### Chapter 7 - Advanced CSS
-  How the browser step by step render our website template.
- What is [minified CSS](https://en.wikipedia.org/wiki/Minification_%28programming%29).
- Layout website as we want with [flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/), flexbox [playaround](https://flexboxfroggy.com/) and help-along [resource](https://darekkay.com/dev/flexbox-cheatsheet.html).
- CSS3's properties [browser support](https://www.w3schools.com/CSSref/css3_browsersupport.asp) or check it [here](https://caniuse.com/)
- Simple animation with transition and transform properties of CSS. Practice it [here](https://robots.thoughtbot.com/transitions-and-transforms).
