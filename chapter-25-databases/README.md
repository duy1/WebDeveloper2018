
### Chapter 25:  Databases.
- Introduction to [database](https://medium.com/omarelgabrys-blog/database-introduction-part-1-4844fada1fb0), what is it ? what does it do and when do we need it?
- [Install](https://www.codecademy.com/articles/sql-commands) Postgresq - the world's most advanced open-source relational database technology.
- Manipulate database with [SQL](https://www.codecademy.com/articles/sql-commands).
- [Practice](https://www.codeschool.com/courses/try-sql) SQL.
