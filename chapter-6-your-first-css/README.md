### Chapter 6 - CSS Properties
-  The 3 ways to link HTML file and CSS file.
-  Some source/tool for leaning CSS.
	- [CSS properties](https://css-tricks.com/).
	- CSS [color design](http://paletton.com/#uid=12R0u0kllllOjmoOjsM4fdU4fki)
- About CSS [selector specificity](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity), which decide the order that css rules are applied.
- A [tool](https://specificity.keegan.st/) for calculating CSS specifility.
- Link [external fonts](https://fonts.google.com/) to use them in your website.
- Introduce some CSS properties that used often.
- CSS [quiz](https://www.w3schools.com/css/css_quiz.asp).
