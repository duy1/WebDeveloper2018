### Chapter 8 - Bootstrap 4, Templates, And Building Your Startup Landing Page
-  What is [Bootstrap](https://getbootstrap.com/) & How to [download](https://getbootstrap.com/docs/4.1/getting-started/download/) and include Bootstrap in your project.
- Some famous sources for customized Bootstrap-based themes [Creative Tim](https://www.creative-tim.com/), [Startupbootstrap](%5Bsources%5D%28https://www.creative-tim.com/%29).
- Create your first bootstrap project called "something-you-like".
- Introduce Git and Github.
- Put your first project on Github, this is [mine](https://github.com/duyphaphach/my-first-repo/commits/master).
- An other useful css library call [Animate.css](https://daneden.github.io/animate.css/) for easy-to-use css animations.
- Some animation css [libraries](https://www.creativebloq.com/advice/5-great-css-animation-resources) that may interest you and help you a lot along your career.
