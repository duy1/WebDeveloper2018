### Chapter 12: Advanced JavaScript.
- [Scope-related definitions](https://toddmotto.com/everything-you-wanted-to-know-about-javascript-scope/) in Javascript.
- Flow control with if else, switch and [ternary operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator).
- Advanced Javascipt array method:
  - [`filter`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter).
  - [`map`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map).
  - [`reduce`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce).

- Advanced Javascipt function method:
  - [Arrow function](https://www.sitepoint.com/es6-arrow-functions-new-fat-concise-syntax-javascript/).
  - [Currying](https://blog.benestudio.co/currying-in-javascript-es6-540d2ad09400).
  - [Function Composition](https://www.gideonpyzer.com/blog/javascript-function-composition-and-pipelines/).
- What is [reference type](https://docstore.mik.ua/orelly/webprog/jscript/ch04_04.htm), [differnce](https://youtu.be/hDT3IbvH-9I) between `scope` and `context`
- [New features](https://www.quora.com/What-features-will-be-in-ES7-and-ES8) in ES7 and ES8
- Debugging with Javascript [debugger](https://www.w3schools.com/jsref/jsref_debugger.asp).
- [How](https://blog.sessionstack.com/how-does-javascript-actually-work-part-1-b0bacc073cf) Javascript works (with Call Stack,  Web Api call back and Event Loop)
- References:
	- [You don't know JS](https://github.com/getify/You-Dont-Know-JS).
	- Modern Javascript [tutorial](http://javascript.info/).
	- [Think](https://medium.freecodecamp.org/how-to-think-like-a-programmer-lessons-in-problem-solving-d1d8bf1de7d2) like a programmer
